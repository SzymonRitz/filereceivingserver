package clientUtils;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.file.Path;
import java.text.DecimalFormat;

public class FileSender {

	public void send(Path file, Socket socket, BufferedReader inPut, PrintWriter outPut) {

		try {
			outPut.println(file.toFile());
			DataInputStream in = new DataInputStream(new FileInputStream(file.toString()));
			DataOutputStream out = new DataOutputStream(socket.getOutputStream());

			byte[] b = new byte[8192];
			int count = 0;
			long bytesReaded = 0;
			System.out.print("Sending: " + file.getFileName() + "	");
			long start = System.currentTimeMillis(); // kiedy zacząłem wysyłać

			do {
					count = in.read(b);
				out.write(b, 0, count);
				outPut.println(count);// ile wysłałem
				System.out.println("I've read: " + count);
//				System.out.println("I've already sent: " + bytesReaded);
				bytesReaded += count;
				long end = System.currentTimeMillis(); // kiedy skończyłem wywyłać
//				double kbps = (double) bytesReaded / (end - start);
				double kbps = (double) bytesReaded / (end - start);
//				while(inPut.readLine().equals("resend")){
//				    out.write(b, 0, count); // ponowne wysyłanie
//				    System.out.println("Resent data package.");
//				}
			
				if (kbps == Double.POSITIVE_INFINITY)
					kbps = 0;
				else
					kbps = kbps / 8;
				if (kbps < 1000)
					System.out.println("Speed: " + new DecimalFormat("###.##").format(kbps) + " Kb/s"); // transfer w Kb
				else {
					kbps = kbps / 1000;
					System.out.println("Speed: " + new DecimalFormat("###.##").format(kbps) + " Mb/s"); // transfer w Mb
				}

			} while (count == 8192);

			System.out.println("Done"); // ukończyłem wysyłanie pliku
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

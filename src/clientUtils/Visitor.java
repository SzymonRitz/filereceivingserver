package clientUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;


public class Visitor {
	Socket socket;
	BufferedReader inPut;
	PrintWriter outPut;

	public Visitor(Socket socket, BufferedReader inPut, PrintWriter outPut) throws IOException {
		this.socket = socket;
		this.inPut = inPut;
		this.outPut = outPut;

	}

	public void visitAndSend(Path startDir) throws IOException {

		Files.walkFileTree(startDir, new SimpleFileVisitor<Path>() { // odwiedza poszczegulne pliki
			public FileVisitResult visitFile(Path file, BasicFileAttributes s) {
				try {
					FileSender fs = new FileSender(); // wysyłanie
					fs.send(file, socket, inPut, outPut); //wyślij
				} catch (Exception e) {
					System.out.println(e);
				}

				return FileVisitResult.CONTINUE;
			}

		});

	}

}

package serverUtils;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ServerThread extends Thread {
	private Socket transferSocket;
	private Socket communicationSocket;
	private Path direction;
	boolean flag = false; //pomocnicza flaga  
	boolean isIn = false;

	public ServerThread(Socket transferSocket, Socket communicationSocket, Path dir) {
		super();
		this.transferSocket = transferSocket;
		this.communicationSocket = communicationSocket;
		this.direction = dir;
	}

	public void run() {
		try {

			BufferedReader inPut = new BufferedReader(new InputStreamReader(communicationSocket.getInputStream()));
			PrintWriter outPut = new PrintWriter(communicationSocket.getOutputStream(), true);

			System.out.println("Connection established.");

			outPut.println("ready"); // komunikowanie o gotowości
			System.out.println("Waiting for client...");

			if (inPut.readLine().equals("ready")) { //jeśli klient odpowiedział 
				Path readedFile = Paths.get(inPut.readLine()); // pobieranie początkowej ścierzki
				DataInputStream in = new DataInputStream(transferSocket.getInputStream()); // socket na pobieranie bajtów
				byte[] b = new byte[8192];
				int readbytes = 1;// ile przeczytałem na początku randomowa wartość
				int sentbytes = 8192;
				long savedbytes = 0;
				long freeSpace = 0;

				while ((sentbytes > 0) || (savedbytes < freeSpace)) { //dopuki paczka została przesłana albo jest mmiejsce na dysku
					String pathTmp = inPut.readLine();
					File finalFile = new File(direction.resolve(readedFile.relativize(Paths.get(pathTmp))).toString()); // przemianowanie ścierzki na docelową
					freeSpace = finalFile.getFreeSpace();// wolne miejsce na dysku
					Paths.get(finalFile.getParent()).toFile().mkdirs();//tworzenie nadkatalogów pliku 
					System.out.println(finalFile.getName());
					if (!finalFile.exists())
						finalFile.createNewFile();
					else
						System.out.println("This file already exists.");

					DataOutputStream out = new DataOutputStream(new FileOutputStream(finalFile));

					do {
						readbytes = in.read(b);
						sentbytes = Integer.parseInt(inPut.readLine()); // ile zostało wysłanych
						
						System.out.println("I've got: " + readbytes);
						System.out.println("I should get: " + sentbytes);
						
						
//						if (readbytes != sentbytes) {// jeżeli wysłane nie są równe przesłanym
//							System.out.println("I got a package with different content");
//							while(readbytes != sentbytes){
//							      outPut.println("resend");
//							      System.out.println("Asking to resend read: "+readbytes+" should be: "+ sentbytes);
//							      readbytes = in.read(b);
//							}
//						}else outPut.println();//wysyła jeśli wszystko poszło ok
						out.write(b,0,readbytes);
						savedbytes +=readbytes;
					} while (sentbytes == 8192);
					System.out.println("Done");
					flag = true;
					out.close();
				}
				if (savedbytes >= freeSpace) {
					flag = false;
					System.out.println("Not enough space");
				}

			} else {
				System.out.println("I received the wrong response.\nAborting...");
				flag = false;
			}
		} catch (IOException e1) {

		}

		if (flag)
			System.out.println("I received all files.");

		try {
			transferSocket.close();
			communicationSocket.close();
		} catch (IOException e) {

		}

	}
}
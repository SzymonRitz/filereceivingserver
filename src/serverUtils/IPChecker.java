package serverUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IPChecker {
	private List<String> IPs = new ArrayList<>();

	public IPChecker() {

	}

	public void chceck(String address) throws Exception {
		boolean flag = false;
		boolean trueflag = false;
		address = address.substring(1);
		System.out.println(address);
		for (String string : IPs) {
			flag = address.equals(string);
			System.out.println(flag);
			if (flag)
				trueflag = flag;
		}
		if (!trueflag)
			throw new Exception("This IP address is unknown");
	}

	public void enterTheListOfIP() {
		Scanner sc = null;
		try {
			String list;
			System.out.println("Enter the list of IP addresses: (IP IP IP)");
			// String list = new BufferedReader(new
			// InputStreamReader(System.in)).readLine();
			sc = new Scanner(System.in);
			list = sc.nextLine();
			String pattern = "[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}";

			Pattern r = Pattern.compile(pattern);
			Matcher m = r.matcher(list);
			int i = 0;
			while (m.find()) {
				i++;
				String s = m.group();
				IPs.add(s);
				System.out.println(s);
			}
			// sc.close();

		} catch (Exception e) {
			// System.out.println("Why it's does not work");
			// e.printStackTrace();
			System.out.println("Enter the the list of IP address again.");
			enterTheListOfIP();
		}
	}

	public List<String> getIPs() {
		return IPs;
	}

	public void setIPs(List<String> iPs) {
		IPs = iPs;
	}

}

package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

import clientUtils.Visitor;

public class Client {

	public static void main(String args[]) {
		String address = enterTheIP();
		int port = enterThePort();
		Path path = enterThePath();
		final BufferedReader inPut;
		final PrintWriter outPut;
		Socket transferSocket = null;
		Socket communicationSocket = null;
		try {
			transferSocket = new Socket(address, port);
			communicationSocket = new Socket(address, port + 1);
		} catch (UnknownHostException e) {
			System.out.println("Unknown host");
			System.exit(-1);
		} catch (IOException e) {
			System.out.println("No I/O");
			System.exit(-1);
		}
		try {
			System.out.println("IP: " + InetAddress.getLocalHost().getHostAddress().toString());
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Komunikacja
		try {
			inPut = new BufferedReader(new InputStreamReader(communicationSocket.getInputStream())); //pobieranie komunikatów
			outPut = new PrintWriter(communicationSocket.getOutputStream(), true);//wysyłanie komunikatów
			System.out.println("The connection established.");
			outPut.println("ready");
			System.out.println("Waiting for server...");
			if (inPut.readLine().equals("ready")) {
				System.out.println("Server is ready.");
				outPut.println(path.toString());
				Visitor visitor = new Visitor(transferSocket, inPut, outPut); // wywołanie vizytora plików
				visitor.visitAndSend(path);
			} else {
				System.out.println("I received the wrong response.\nAborting...");
				System.exit(-1);
			}

		} catch (Exception e) {
			System.out.println("Error during communication.");
			System.exit(-1);
		}

		try {
			transferSocket.close();
		} catch (IOException e) {
			System.out.println("Cannot close the socket.");
			System.exit(-1);
		}

	}

	static int enterThePort() {
		int port = 0;
		try {
			System.out.println("Enter the port:");
			port = Integer.parseInt(new BufferedReader(new InputStreamReader(System.in)).readLine());

		} catch (Exception e) {
			System.out.println("Enter the port again.");
			enterThePort();
		}
		return port;

	}

	static String enterTheIP() {
		Scanner sc = null;
		String address = null;
		try {
			System.out.println("Enter the IP address:");
			sc = new Scanner(System.in);
			address = sc.next("[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}");
		} catch (Exception e) {
			System.out.println("Enter the IP address again.");
			enterTheIP();
		}
		return address;

	}

	static Path enterThePath() {
		Path path = null;
		BufferedReader br = null;
		try {
			System.out.println("Enter the path to send:");
			br = new BufferedReader(new InputStreamReader(System.in));
			path = Paths.get(br.readLine());
		} catch (IOException e1) {
			System.out.println("Enter the path again:");
			enterThePath();
			e1.printStackTrace();
		}

		return path;
	}

}

package main;

import java.io.*;
import java.net.*;
import java.nio.file.Path;
import java.nio.file.Paths;

import serverUtils.IPChecker;
import serverUtils.ServerThread;

public class Server {

	public static void listenSocket() {
		ServerSocket serverSocket = null;
		ServerSocket communicationSocket = null;
		Socket client = null;
		Socket clientComm = null;
		IPChecker checker = new IPChecker();
		Path direction = enterTheDirection();

		try {
			serverSocket = new ServerSocket(0);
			communicationSocket = new ServerSocket(serverSocket.getLocalPort() + 1);
		} catch (IOException e) {
			System.out.println(e);
			System.exit(-1);
		}
		System.out.println("Server is listening on port " + serverSocket.getLocalPort());
		try {
			System.out.println("IP: " + InetAddress.getLocalHost().getHostAddress().toString());
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		while (true) {
			try {
				checker.enterTheListOfIP(); // podaj adres IP
				client = serverSocket.accept(); // zatwierdzanie połączenia wymiany danych
				checker.chceck(client.getInetAddress().toString());
				clientComm = communicationSocket.accept();// zatwierdzanie połączenia protokołowego
			} catch (IOException e) {
				System.out.println("Accept failed");
				System.exit(-1);
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(-1);
			}

			(new ServerThread(client, clientComm, direction)).start();
		}
	}

	public static void main(String[] args) {
		listenSocket();
	}

	static Path enterTheDirection() {
		Path dir = null;
		BufferedReader br = null;
		try {
			System.out.println("Specify a folder where do you want to save files:");
			br = new BufferedReader(new InputStreamReader(System.in));
			dir = Paths.get(br.readLine());
		} catch (IOException e1) {
			System.out.println("Enter the path again:");
			enterTheDirection();
			e1.printStackTrace();
		}

		if (!dir.toFile().exists()) {
			try {
				dir.toFile().createNewFile();
				System.out.println("The directory does not exist so I created it.");
			} catch (IOException e) {
				System.out.println("Something goes wrong with creating directory.");
				e.printStackTrace();
			}
		}
		return dir;
	}

}